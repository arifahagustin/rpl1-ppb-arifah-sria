import 'package:flutter/material.dart';
//import 'package:chart_model/tugas/telegram/Telegram.dart';
import 'package:chart_model/tugas/loginpage/LoginScreen.dart';
import 'package:chart_model/tugas/loginpage/HomeScreen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Login',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LoginScreen(),
    );
  }
}
