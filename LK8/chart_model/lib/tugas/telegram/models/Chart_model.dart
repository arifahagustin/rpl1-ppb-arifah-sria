import 'package:flutter/cupertino.dart';

class ChartModel {
  final String? name;
  final String? massage;
  final String? time;
  final String? profileUrl;

  ChartModel({this.name, this.massage, this.time, this.profileUrl});
}

final List<ChartModel> items = [
  ChartModel(
      name: 'abay',
      massage: 'Bub',
      time: '13.43',
      profileUrl:
          'https://images.tokopedia.net/blog-tokopedia-com/uploads/2021/11/Biodata-Haruto-Treasure.jpg'),
  ChartModel(
      name: 'jeki',
      massage: 'dam,marvin cakep',
      time: 'Tue',
      profileUrl:
          'https://asset.kompas.com/crops/YPuubBXEdkwzIGKkupV0u7x2boA=/659x184:1264x587/750x500/data/photo/2020/11/19/5fb5de933775c.png'),
  ChartModel(
      name: 'retha pret dut',
      massage: 'dam, mau malio',
      time: 'Feb 9',
      profileUrl:
          'https://assets.promediateknologi.com/crop/0x0:0x0/0x0/webp/photo/2023/01/09/2559515076.jpg'),
];
