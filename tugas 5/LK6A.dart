void main(List<String> args) {
  print(rangeWithStep(1, 10, 2));
  print(rangeWithStep(11, 23, 3));
  print(rangeWithStep(5, 2, 1));
}

rangeWithStep(startNum, finishNum, step) {
  var rangeArr = [];
  if (startNum > finishNum) {
    var currentNum = startNum;
    for (var i = 0; currentNum >= finishNum; i++) {
      rangeArr.add(currentNum);
      currentNum -= step;
    }
  } else if (startNum < finishNum) {
    var currentNum = startNum;
    for (var i = 0; currentNum <= finishNum; i++) {
      rangeArr.add(currentNum);
    }
  } else {
    return 1;
  }
  return rangeArr;
}
