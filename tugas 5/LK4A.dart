void main(List<String> args) {
  faktorial();
}

faktorial() {
  int faktorial = 1;
  int n = 5;
  if (n <= 0) {
    print("1");
  } else {
    for (int i = 1; i <= n; i++) {
      faktorial *= i;
    }
    print("Hasil faktorial dari " +
        n.toString() +
        " adalah " +
        faktorial.toString());
  }
}
