void main(List<String> args) {
  for (int i = 1; i <= 20; i++) {
    if (i % 5 == 0) {
      print("$i kelipatan 5");
    } else if (i % 2 == 0) {
      print("$i Genap");
    } else {
      print("$i Ganjil");
    }
  }
}
